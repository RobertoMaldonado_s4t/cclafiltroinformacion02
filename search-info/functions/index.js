const firebase = require("firebase");
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });
const functions = require('firebase-functions');

// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyAo2-zSRKdhxbkEWmdkCf2R0V_ZuPGZ3pY",
    authDomain: "misdocumentosafiliado.firebaseapp.com",
    projectId: "misdocumentosafiliado"

});

var db = firebase.firestore();


function formatDate(date) {

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if (month < 10) {
        month = '0' + month;
    }

    if (day < 10) {
        day = '0' + day;
    }

    return year + '-' + month + '-' + day
}

exports.buscarFiltros = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        const { body } = req;

        /*
       const isValidMessage = body.rut;

       if (!isValidMessage) {

           var respuesta = {
               error: true,
               codigo: 400,
               mensaje: 'parametros de entrada invalidos'
           }

           return res.send({ respuesta });
       }
       */

        /*
            var _rut = req.query.rut;
            var _motivo = req.query.motivo;
            var _fecha = req.query.fecha;
            var _estado = req.query.estado;
        */

        var _rut = body.rut;
        var _motivo = body.motivo;
        var _fecha = body.fecha;
        var _estado = body.estado;

        console.log(_rut + " " + _motivo + " " + _fecha)

        documento = {
            archivo: "",
            fecha: "",
            fileName: "",
            tipoDocDescrip: "",
            tipoDocumento: ""
        }

        usuario = {
            estado: "",
            fecha: "",
            motivoEnvio: "",
            estado: "",
            infoComplementaria: "",
            documentos: [documento]

        };

        var respuesta = new Array();

        console.log(_motivo);
        console.log(_rut);

        var citiesRef = db.collection("solicitudes")

        if (!Object.keys(_fecha).length == 0) {

            console.log("entro en fecha - " + _fecha);

            var hora = "T00:00:00-0500";
            var fechaIn = _fecha;

            var fechaMax = new Date(fechaIn);
            fechaMax.setDate(fechaMax.getDate() + 1);
            var fechaMaxFormat = Date.parse(formatDate(fechaMax) + hora);
            var fechaMinFormat = Date.parse(fechaIn + hora);

            //var fecha = Date.parse('2020-04-13T00:00:00-0500');

            //console.log("fecha fechaMinFormat - " + fechaMinFormat);
            //console.log("fecha fechaMaxFormat - " + fechaMaxFormat);
            //resp = { fechaMinFormat, fechaMaxFormat }
            //return res.send({ resp });

        }

        var query1 = citiesRef;
        var query2 = "";

        if (!Object.keys(_motivo).length == 0) {
            console.log("entro motivo - " + _motivo);
            query2 = query1.where("motivoEnvio", "==", _motivo);
        }

        if (!Object.keys(_estado).length == 0 && Object.keys(query2).length == 0) {
            if (parseInt(_estado) >= 1 && parseInt(_estado) <= 3) {
                console.log("entro estado A - " + parseInt(_estado));
                query2 = query1.where("estado", "==", parseInt(_estado));
            }
        } else if (!Object.keys(_estado).length == 0 && Object.keys(query2).length > 0) {
            if (parseInt(_estado) >= 1 && parseInt(_estado) <= 3) {
                console.log("entro estado B - " + parseInt(_estado));
                query2 = query2.where("estado", "==", parseInt(_estado));
            }
        }

        if (!Object.keys(_rut).length == 0 && Object.keys(query2).length == 0) {
            console.log("entro en rut A - " + _rut);
            query2 = query1.where("rutAfiliado", "==", _rut);
        } else if (!Object.keys(_rut).length == 0 && Object.keys(query2).length > 0) {
            console.log("entro rut B - " + _fecha);
            query2 = query2.where("rutAfiliado", "==", _rut);
        }

        if (Object.keys(query2).length == 0) {
            console.log("no entro");
            query2 = citiesRef;
        }

        query2.get().then(function(querySnapshot) {

                querySnapshot.forEach(function(doc) {
                    const post = doc.data() || {};

                    usuario = {

                        id: post.id || {},
                        estado: post.estado || {},
                        fecha: post.fecha || {},
                        fechaNum: post.createdAt || {},
                        motivoEnvio: post.motivoEnvio || {},
                        rutAfiliado: post.rutAfiliado || {},
                        infoComplementaria: post.infoComplementaria || {},
                        documentos: post.documentos

                    }

                    if (!Object.keys(_fecha).length == 0) {

                        if (parseInt(post.createdAt) > parseInt(fechaMinFormat) && parseInt(post.createdAt) < parseInt(fechaMaxFormat)) {
                            respuesta.push(usuario);
                        }

                    } else {
                        respuesta.push(usuario);
                    }

                    //var docs = querySnapshot.docs.map(doc => doc.data());

                });

                return res.send({ respuesta });

            })
            .catch(function(error) {

                var respuesta = {
                    error: true,
                    codigo: 500,
                    mensaje: 'error al filtar la informacion'
                }

                return res.send({ respuesta });

            });

    });
});